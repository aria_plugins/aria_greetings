## Core (https://gitlab.com/aria_plugins/core/)
all:	aria-plugin.mk
aria-plugin.mk:
	wget -qO - https://gitlab.com/aria_plugins/core/raw/master/mk/install.sh | bash
-include aria-plugin.mk
## Here you can add custom commands and overrides
